// I am le Register Controller
app.controller(
  "UsersRegisterCtrl", ['$scope', '$rootScope', '$filter', '$timeout',
    'DMSRestangular', '$state', 'localStorageService', 'MySessionService', '$auth', 'toastr',
    function(scope, rootScope, filter, timeout, DMSRestangular, state,
      localStorageService, MySessionService, auth, toastr) {

      scope.handleRegBtnClick = function() {
      auth.submitRegistration(scope.registrationForm)
        .then(function(resp) { 
          // handle success response
         state.go('/');
         toastr.success("Registration success: ");

        })
        .catch(function(resp) { 
         toastr.error("Registration success: ");
          // handle error response
        console.log(resp.errors);
        });
    };

    scope.$on('auth:registration-email-error', function(ev, reason) {
    toastr.info("Registration failed: " + reason.errors[0]);
    });
    }
  ]
);