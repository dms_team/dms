// I am ze deanerys Controller
app.controller(
  "deaneriesCtrl", ['$scope', '$rootScope', '$filter', '$timeout',
    'DMSRestangular', '$state', 'localStorageService', 'MySessionService', 'toastr',
    function(scope, rootScope, filter, timeout, DMSRestangular, state,
      localStorageService, MySessionService, toastr) {
      var Deaneries = DMSRestangular.all('deaneries');
      getDeaneryCount();
      rootScope.user = MySessionService.getLoggedUser();

      scope.getDeanery = function getDeanery(newDeanery) {
        scope.deaneryProfile = newDeanery;
        state.go('location.deaneries.view');
      };

      scope.getDeaneries = function getDeaneries() {
        Deaneries.customGET('').then(function(deaneries) {
          scope.rowCollection = deaneries;
          scope.displayedCollection = [].concat(scope.rowCollection);
        });
      };

      scope.login = function login() {
        rootScope.user = [];
        var user = DMSRestangular.one('user').one('username', scope.formData
          .username).one('password', scope.formData.password).one(
          'format', 'json');
        // This will query /accounts and return a promise.
        user.customGET('').then(function(userObj) {
          localStorageService.set('meds_user', userObj);
          state.go('users');

        });
      };

      function getDeaneryCount() {
        Deaneries.customGET('').then(function(deaneries) {
          scope.records = deaneries.length;
          scope.recordsPerPage = 5;
          scope.pages = Math.ceil(scope.records / scope.recordsPerPage);
        });
      }

      scope.setStatus = function setStatus(status) {
        scope.status = status;
        if (status == 'add') {
          scope.deaneryProfile = [];
        }
      };
      scope.newDeanery = function newDeanery() {
        deanery = {
              "deanery": {
                  "name":       scope.deaneryProfile.name,
                  "updated_at":  scope.deaneryProfile.updated_at,
                  "created_at":   scope.deaneryProfile.created_at
         }
        };
        console.log(deanery);
        Deaneries.customPOST(deanery);

      };
        
        scope.updateDeanery = function updateDeanery() {
        updatedDeanery = DMSRestangular.one('deaneries', scope.deaneryProfile.id);

        today = new Date();
        year = today.getFullYear();
        month = today.getMonth() + 1;
        day = today.getDay();
        // this.updated_at = year + '-' + month + '-' + day;
        var now = year + '-' + month + '-' + day;
        
        deanery = {
              "deanery": {
              "id":         scope.deaneryProfile.id,
              "name":       scope.deaneryProfile.name,
              "updated_at":  scope.deaneryProfile.updated_at,
              "created_at":   scope.deaneryProfile.created_at
              }
        };
        updatedDeanery.customPUT(deanery).then(function(response){
        toastr.info('Update Successful', 'Awesome!'); 
        }, function(response) {
        toastr.danger('Update was not successful', 'Wow!'); 
        });
        console.log(scope.Profile.id);
      };

    }
  ]
);