// I am ze archdioceses Controller
app.controller(
  "archdiocesesCtrl", ['$scope', '$rootScope', '$filter', '$timeout',
    'DMSRestangular', '$state', 'localStorageService', 'MySessionService', 'toastr',
    function(scope, rootScope, filter, timeout, DMSRestangular, state,
      localStorageService, MySessionService, toastr) {
      var Archdioceses = DMSRestangular.all('archdioceses');
      getArchdioceseCount();
      rootScope.user = MySessionService.getLoggedUser();

      scope.getArchdiocese = function getArchdiocese(newArchdiocese) {
        scope.archdioceseProfile = newArchdiocese;
        state.go('location.archdioceses.view');
      };

      scope.getArchdioceses = function getArchdioceses() {
        Archdioceses.customGET('').then(function(archdioceses) {
          scope.rowCollection = archdioceses;
          scope.displayedCollection = [].concat(scope.rowCollection);
        });
      };

      scope.login = function login() {
        rootScope.user = [];
        var user = DMSRestangular.one('user').one('username', scope.formData
          .username).one('password', scope.formData.password).one(
          'format', 'json');
        // This will query /accounts and return a promise.
        user.customGET('').then(function(userObj) {
          localStorageService.set('meds_user', userObj);
          state.go('users');

        });
      };

      function getArchdioceseCount() {
        Archdioceses.customGET('').then(function(archdioceses) {
          scope.records = archdioceses.length;
          scope.recordsPerPage = 5;
          scope.pages = Math.ceil(scope.records / scope.recordsPerPage);
        });
      }

      scope.setStatus = function setStatus(status) {
        scope.status = status;
        if (status == 'add') {
          scope.archdioceseProfile = [];
        }
      };
      scope.newArchdiocese = function newArchdiocese() {
        archdiocese = {
              "archdiocese": {
                  "name":       scope.archdioceseProfile.name
                  
         }
        };
        console.log(archdiocese);
        Archdioceses.customPOST(archdiocese);

      };
        
        scope.updateArchdiocese = function updateArchdiocese() {
        updatedArchdiocese = DMSRestangular.one('archdioceses', scope.archdioceseProfile.id);

        today = new Date();
        year = today.getFullYear();
        month = today.getMonth() + 1;
        day = today.getDay();
        // this.updated_at = year + '-' + month + '-' + day;
        var now = year + '-' + month + '-' + day;
        
        archdiocese = {
              "archdiocese": {
              "id":         scope.archdioceseProfile.id,
              "name":       scope.archdioceseProfile.name,
              
              }
        };
        updatedArchdiocese.customPUT(archdiocese).then(function(response){
        toastr.info('Update Successful', 'Awesome!'); 
        }, function(response) {
        toastr.danger('Update was not successful', 'Wow!'); 
        });
        console.log(scope.Profile.id);
      };

    }
  ]
);