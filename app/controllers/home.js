app.controller("HomeCtrl", [ '$rootScope', '$scope' , function(rootScope, scope) {

    // Title of the page
    
      rootScope.title = 'DMS | Home';
      
    // Get the countries geojson data from a JSON
    
   /* $http.get("examples/json/JPN.geo.json").success(function(data, status) {
        angular.extend($scope, {
            geojson: {
                data: data,
                style: {
                    fillColor: "green",
                    weight: 2,
                    opacity: 1,
                    color: 'white',
                    dashArray: '3',
                    fillOpacity: 0.7
                }
            }
        });
    });
   */ 
   
    angular.extend(scope, {
      nairobi: {
        lat: -1.2833,
        lng: 36.8167,
        zoom: 12
      },
      markers: {
            baptist_church: {
                lat: -1.297839,
                lng: 36.800968,
                message: "Nairobi Baptist Church",
                draggable: false
                },
            all_saints_church: {
                lat: -1.289012,
                lng: 36.814264,
                message: "All Saints Cathedral Nairobi",
                draggable: false
                },
            house_of_grace: {
                lat: -1.310287,
                lng: 36.817931,
                message: "House of Grace",
                draggable: false
                },
            mavuno_church: {
                lat: -1.320463,
                lng: 36.836364,
                message: "Mavuno Church",
                draggable: false
                },
            st_andrews_church: {
                lat: -1.282568,
                lng: 36.813478,
                message: "St. Andrews Church",
                draggable: false
                },
            sda_church: {
                lat: -1.223348,
                lng: 36.781698,
                message: "Nairobi Central SDA Church",
                draggable: false
                }
            },
      layers: {
        baselayers: {
           mapbox_classic: {
             name: 'Mapbox Classic',
             url: 'https://a.tiles.mapbox.com/v4/{mapid}/{z}/{x}/{y}.png?access_token={apikey}',
             type: 'xyz',
             layerOptions: {
               apikey: 'pk.eyJ1IjoidmljNzgiLCJhIjoiYjdkMzUwM2IyMjRiZWIxYzQ1Mjk5YTBjYmI2YzI2ZDMifQ.Tsm2SZhOm0IoY7aHzi0CAA',
               mapid: 'vic78.n4gpl66p'
             }
          },
           mapbox_light: {
             name: 'Mapbox Satellite',
             url: 'https://a.tiles.mapbox.com/v4/{mapid}/{z}/{x}/{y}.png?access_token={apikey}',
             type: 'xyz',
             layerOptions: {
               apikey: 'pk.eyJ1IjoidmljNzgiLCJhIjoiYjdkMzUwM2IyMjRiZWIxYzQ1Mjk5YTBjYmI2YzI2ZDMifQ.Tsm2SZhOm0IoY7aHzi0CAA',
               mapid: 'vic78.mphf9k8h'
             }
          },
          mapbox_dark: {
             name: 'Mapbox Dark',
             url: 'https://a.tiles.mapbox.com/v4/{mapid}/{z}/{x}/{y}.png?access_token={apikey}',
             type: 'xyz',
             layerOptions: {
               apikey: 'pk.eyJ1IjoidmljNzgiLCJhIjoiYjdkMzUwM2IyMjRiZWIxYzQ1Mjk5YTBjYmI2YzI2ZDMifQ.Tsm2SZhOm0IoY7aHzi0CAA',
               mapid: 'vic78.f679a5b4'
             }
          },
          mapbox_hike: {
             name: 'Mapbox Hike',
             url: 'https://a.tiles.mapbox.com/v4/{mapid}/{z}/{x}/{y}.png?access_token={apikey}',
             type: 'xyz',
             layerOptions: {
               apikey: 'pk.eyJ1IjoidmljNzgiLCJhIjoiYjdkMzUwM2IyMjRiZWIxYzQ1Mjk5YTBjYmI2YzI2ZDMifQ.Tsm2SZhOm0IoY7aHzi0CAA',
               mapid: 'vic78.n2jnmffn'
             }
          },
          mapbox_pirates: {
             name: 'Mapbox Pirates',
             url: 'https://a.tiles.mapbox.com/v4/{mapid}/{z}/{x}/{y}.png?access_token={apikey}',
             type: 'xyz',
             layerOptions: {
               apikey: 'pk.eyJ1IjoidmljNzgiLCJhIjoiYjdkMzUwM2IyMjRiZWIxYzQ1Mjk5YTBjYmI2YzI2ZDMifQ.Tsm2SZhOm0IoY7aHzi0CAA',
               mapid: 'vic78.n2jo5m0k'
             }
          },
           mapbox_wheatpaste: {
             name: 'Mapbox Wheat Paste',
             url: 'https://a.tiles.mapbox.com/v4/{mapid}/{z}/{x}/{y}.png?access_token={apikey}',
             type: 'xyz',
             layerOptions: {
               apikey: 'pk.eyJ1IjoidmljNzgiLCJhIjoiYjdkMzUwM2IyMjRiZWIxYzQ1Mjk5YTBjYmI2YzI2ZDMifQ.Tsm2SZhOm0IoY7aHzi0CAA',
               mapid: 'vic78.mp1jp046'
             }
            }
          }
      }
    });
}]);



