// I am ze dioceses Controller
app.controller(
  "diocesesCtrl", ['$scope', '$rootScope', '$filter', '$timeout',
    'DMSRestangular', '$state', 'localStorageService', 'MySessionService', 'toastr',
    function(scope, rootScope, filter, timeout, DMSRestangular, state,
      localStorageService, MySessionService, toastr) {
      var Dioceses = DMSRestangular.all('dioceses');
      getDioceseCount();
      rootScope.user = MySessionService.getLoggedUser();

      scope.getDiocese = function getDiocese(newDiocese) {
        scope.dioceseProfile = newDiocese;
        state.go('location.dioceses.view');
      };

      scope.getDioceses = function getDioceses() {
        Dioceses.customGET('').then(function(dioceses) {
          scope.rowCollection = dioceses;
          scope.displayedCollection = [].concat(scope.rowCollection);
        });
      };

      scope.login = function login() {
        rootScope.user = [];
        var user = DMSRestangular.one('user').one('username', scope.formData
          .username).one('password', scope.formData.password).one(
          'format', 'json');
        // This will query /accounts and return a promise.
        user.customGET('').then(function(userObj) {
          localStorageService.set('meds_user', userObj);
          state.go('users');

        });
      };

      function getDioceseCount() {
        Dioceses.customGET('').then(function(dioceses) {
          scope.records = dioceses.length;
          scope.recordsPerPage = 5;
          scope.pages = Math.ceil(scope.records / scope.recordsPerPage);
        });
      }

      scope.setStatus = function setStatus(status) {
        scope.status = status;
        if (status == 'add') {
          scope.dioceseProfile = [];
        }
      };
      scope.newDiocese = function newDiocese() {
        diocese = {
              "diocese": {
                  "name":       scope.dioceseProfile.name,
                  "updated_at":  scope.dioceseProfile.updated_at,
                  "created_at":   scope.dioceseProfile.created_at
         }
        };
        console.log(diocese);
        Dioceses.customPOST(diocese);

      };
        
        scope.updateDiocese = function updateDiocese() {
        updatedDiocese = DMSRestangular.one('dioceses', scope.dioceseProfile.id);

        today = new Date();
        year = today.getFullYear();
        month = today.getMonth() + 1;
        day = today.getDay();
        // this.updated_at = year + '-' + month + '-' + day;
        var now = year + '-' + month + '-' + day;
        
        diocese = {
              "diocese": {
              "id":         scope.dioceseProfile.id,
              "name":       scope.dioceseProfile.name,
              "updated_at":  scope.dioceseProfile.updated_at,
              "created_at":   scope.dioceseProfile.created_at
              }
        };
        updatedDiocese.customPUT(diocese).then(function(response){
        toastr.info('Update Successful', 'Awesome!'); 
        }, function(response) {
        toastr.danger('Update was not successful', 'Wow!'); 
        });
        console.log(scope.Profile.id);
      };

    }
  ]
);