// I am le Members Controller
app.controller(
  "membersCtrl", ['$scope', '$rootScope', '$filter', '$timeout',
    'DMSRestangular', '$state', 'localStorageService', 'MySessionService',
    function(scope, rootScope, filter, timeout, DMSRestangular, state,
      localStorageService, MySessionService) {
      var Members = DMSRestangular.all('members');
      getMemberCount();
      rootScope.user = MySessionService.getLoggedUser();

      scope.getMember = function getMember(newMember) {
        scope.memberProfile = newMember;
        state.go('location.members.view');
      };

      scope.getMembers = function getMembers() {
        Members.customGET('').then(function(members) {
          scope.rowCollection = members;
          scope.displayedCollection = [].concat(scope.rowCollection);
        });
      };

      scope.login = function login() {
        rootScope.user = [];
        var user = DMSRestangular.one('user').one('username', scope.formData
          .username).one('password', scope.formData.password).one(
          'format', 'json');
        // This will query /accounts and return a promise.
        user.customGET('').then(function(userObj) {
          localStorageService.set('dms_user', userObj);
          state.go('dashboard');

        });
      };

      function getMemberCount() {
        Members.customGET('').then(function(members) {
          scope.records = members.length;
          scope.recordsPerPage = 5;
          scope.pages = Math.ceil(scope.records / scope.recordsPerPage);
        });
      }

      scope.setStatus = function setStatus(status) {
        scope.status = status;
        if (status == 'add') {
          scope.memberProfile = [];
        }
      };
      scope.newMember = function newMember() {

     member = {
              "member": {
                  "name":       scope.memberProfile.name,
         }
        };
        console.log(member);
        members.customPOST(member);

      };
        
        scope.updateMember = function updateMember() {
        updatedMember = DMSRestangular.one('members', scope.memberProfile.id);

        today = new Date();
        year = today.getFullYear();
        month = today.getMonth() + 1;
        day = today.getDay();
        // this.updated_at = year + '-' + month + '-' + day;
        var now = year + '-' + month + '-' + day;
        
        member = {
              "member": {
              "id":         scope.memberProfile.id,
              "name":       scope.memberProfile.name
              
              }
        };
        updatedMember.customPUT(member).then(function(response){
        toastr.info('Update Successful', 'Awesome!'); 
        }, function(response) {
        toastr.danger('Update was not successful', 'Wow!'); 
        });
        console.log(scope.Profile.id);
      };

    }
  ]
);